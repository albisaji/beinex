export const STACKED_DATA_1=[
    {month: "Jan", apples: 380, bananas: 520, cherries: 960, dates: 400},
    {month: "Feb", apples: 160, bananas: 540, cherries: 960, dates: 400},
    {month: "March", apples:  640, bananas:  460, cherries: 640, dates: 400},
    {month: "Apr", apples:  320, bananas:  680, cherries: 640, dates: 400}
]
export const STACKED_DATA_2=[
    {month: "Jan", apples: 200, bananas: 150, cherries: 450, dates: 100},
    {month: "Feb", apples: 160, bananas: 200, cherries: 400, dates: 250},
    {month: "March", apples:  640, bananas:  100, cherries: 250, dates: 450},
    {month: "Apr", apples:  110, bananas:  240, cherries: 170, dates: 350}
]